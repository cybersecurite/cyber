---
hide:
  - toc
author: à compléter
title: Stéganographie
---

# Stéganographie

![](https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/ping2.png){: .center }




!!! abstract "Description générale de la catégorie"

    La stéganographie est l'art de masquer une information dans une autre, comme, par exemple, un texte dans un texte, un texte dans une image ou encore une image dans une image.

    Dans cette catégorie, on propose divers challenges permettant de se familiariser avec quelques techniques simples de stéganographie.

    



<hr style="height:5px;color:red;background-color:red;">

!!! note "ASCII Art"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐ 1 flag

        **Objectif :** manipuler un fichier texte pour extraire les données importantes

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/AsciiArt/ascii-text-art.png){: .center }
    
    Votre meilleur agent vient de vous envoyer une information cruciale. Utilisez tout votre savoir-faire pour la décoder 
    au plus vite !

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/AsciiArt/ascii_art/){ .md-button target="_blank" rel="noopener" }


!!! note "Casse-tête chinois (challenge issu de la plateforme <a href='https://www.challenges-kids.fr/index.php' target='_blank'>challenges-kids.fr</a>)"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐ 1 flag

        **Objectif :** manipuler un logiciel à l'aide d'une documentation

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/Challenges_externes/casse_tete.png){: .center }
    
    Cela ressemble à une langue mais est-ce vraiment le cas ?

    Saurez-vous retrouver le message qui y est dissimulé ?

    <a href='https://www.challenges-kids.fr/index.php' target='_blank'>Challenges-kids.fr</a> est une plateforme ouverte à la contribution et d'aide à l'enseignement du Hacking pour les petits et les grands, qui offre de nombreux challenges de difficultés graduées pour progresser.

    [_Accéder au challenge_](https://www.challenges-kids.fr/categories/stegano/chall5/){ .md-button target="_blank" rel="noopener" }


!!! note "Gif"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐ 1 flag

        **Objectif :** comprendre ce qu'est un gif

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/Gif/chall_gif_intro.png){: .center }
    
    La suite du challenge <a href='https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/Remparts/remparts/' target='_blank'>Remparts</a> dans lequel Guillaume met fin aux agissements du fameux hackeur breton TheKalife.

    Un mail... Une pièce jointe... Tout est-il à recommencer ?

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/Gif/gif/){ .md-button target="_blank" rel="noopener" }



!!! note "Image, ma belle image"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐⭐ 3 flags

        **Objectif :** manipuler quelques techniques simples de stéganographie

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/ImageMaBelleImage/image_livre.png){: .center }
    
    Une image peut cacher bien des secrets. Découvrez quelques techniques de stéganographie et faites preuve de toute votre sagacité pour découvrir les messages cachés.

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/ImageMaBelleImage/image_ma_belle_image/){ .md-button target="_blank" rel="noopener" }


!!! note "Image corrompue"

    !!! tip "Présentation"

        **Niveau :** SNT, NSI

        **Difficulté :** ⭐⭐ 1 flag

        **Objectif :** comprendre ce qu'est la signature d'une image

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/ImageCorrompue/image_corrompue.png){: .center }
    
    Un message caché dans une image qui n'en est peut-être pas une... ou pas... Quelques manipulations d'entête devraient 
    faire l'affaire...

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/ImageCorrompue/image_corrompue/){ .md-button target="_blank" rel="noopener" }


!!! note "Musique"

    !!! tip "Présentation"

        **Niveau :** NSI

        **Difficulté :** ⭐⭐⭐ 1 flag

        **Objectif :** manipuler diverses formes de stéganographie

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/Musique/chall_musique.png){: .center }
    
    La suite du challenge <a href='https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/Gif/gif/' target='_blank'>Gif</a> dans lequel Guillaume apprend que le fameux hackeur breton TheKalife s'est enfui et lui donne rendez-vous au Metropolitan Opera de New-York.

    Jeu de piste ou jeu du chat et de la souris ? Tout en musique !

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/Musique/musique/){ .md-button target="_blank" rel="noopener" }



!!! note "QR Code cassé (challenge issu de la plateforme <a href='https://www.challenges-kids.fr/index.php' target='_blank'>challenges-kids.fr</a>)"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐ 1 flag

        **Objectif :** comprendre ce qu'est un QR Code

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/Challenges_externes/qr_code_casse.png){: .center }
    
    Un QR Code qui a une drôle d'allure...

    Saurez-vous retrouver le message qui y est dissimulé ?

    <a href='https://www.challenges-kids.fr/index.php' target='_blank'>Challenges-kids.fr</a> est une plateforme ouverte à la contribution et d'aide à l'enseignement du Hacking pour les petits et les grands, qui offre de nombreux challenges de difficultés graduées pour progresser.

    [_Accéder au challenge_](https://www.challenges-kids.fr/categories/culture/chall2/){ .md-button target="_blank" rel="noopener" }


!!! note "QR Code gagnant (challenge issu de la plateforme <a href='https://www.challenges-kids.fr/index.php' target='_blank'>challenges-kids.fr</a>)"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐ 1 flag

        **Objectif :** comprendre qu'un QR Code peut être malveillant

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/Challenges_externes/qr_code_gagnant.png){: .center }
    
    Scannez moi vite pour obtenir le flag... ou pas...

    Flasher, ou ne pas flasher, telle est la question ...

    <a href='https://www.challenges-kids.fr/index.php' target='_blank'>Challenges-kids.fr</a> est une plateforme ouverte à la contribution et d'aide à l'enseignement du Hacking pour les petits et les grands, qui offre de nombreux challenges de difficultés graduées pour progresser.

    [_Accéder au challenge_](https://www.challenges-kids.fr/categories/culture/chall5/){ .md-button target="_blank" rel="noopener" }


!!! note "Spectrale"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐ 2 flags

        **Objectif :** comprendre qu'un fichier audio peut aussi contenir une image

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/Spectrale/image_spectrale.png){: .center }

    Un groupe de hackers astucieux orchestrent des attaques informatiques complexes et apparemment indétectables.

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/Spectrale/spectrale/){ .md-button target="_blank" rel="noopener" }