---
hide:
  - toc
author: à compléter
title: Algorithmique
---

# Algorithmique

![](https://cybersecurite.forge.apps.education.fr/cyber-2-algo/3.Challenges/ressources_challenges_algo/mdp9.png){: .center }

!!! tip "Tina Nikoukhah"

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-algo/3.Challenges/ressources_challenges_algo/Tina.jpg){: .center }

    Tina Nikoukhah est docteure en mathématiques appliquées et ingénieure en informatique. Elle travaille sur l’élaboration d’outils pour la détection et l’authentification d’images et de vidéos. En parallèle, elle enseigne le traitement d’images à des journalistes. Elle est également impliquée dans la diffusion scientifique auprès du grand public. Voici deux de ses articles publiés sur Interstices :

    - Tout ce que les <a href='https://interstices.info/tout-ce-que-les-algorithmes-de-traitement-dimages-font-pour-nous/' target='_blank'>algorithmes de traitement d’images</a> font pour nous.
    - Les <a href='https://interstices.info/les-traces-de-compression-pour-detecter-les-photomontages/' target='_blank'>traces de compression</a> pour détecter les photomontages.

!!! abstract "Description générale de la catégorie"

    La compréhension des algorithmes sous-jacents aux codes informatique qui ont été implémentés dans certaines applications est souvent un vecteur de nombreuses attaques.
    
    Dans cette catégorie, on propose divers challenges ayant pour but la compréhension à la main de codes informatiques.

!!! warning "Attention"

    Pour que ces challenges aient tout leur intérêt, il est recommandé de ne pas implémenter les algorithmes proposés et de ne pas les exécuter sur machine. Ceux-ci doivent être exécutés à la main.



<hr style="height:5px;color:red;background-color:red;">

!!! note "Blackbeard’s Hidden Treasures (d'après <a href='https://www.101computing.net/'>101computing.net</a>)"

    !!! tip "Présentation"

        **Niveau :** NSI

        **Difficulté :** ⭐⭐ 12 flags

        **Objectif :** travailler les notions de base de l'algorithmique (variables, affectations, boucles) ainsi que les structures de données complexes que sont les chaînes de caractères.

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-algo/3.Challenges/ressources_challenges_algo/BlackbeardsHiddenTreasures/pirate-flag.png){: .center }
    
    Le célèbre pirate Barbe-Noire a caché son trésor en douze lieux différents. Différents fragments de parchemins, qui devraient indiquer ces lieux, viennent tout juste d'être découverts dans une salle obscure d'une bibliothèque. Saurez-vous faire preuve de sagacité et de perspicacité pour les déchiffer et mettre enfin à jour ce fabuleux trésor ?

    !!! warning "Challenge en anglais" 
    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-algo/3.Challenges/ressources_challenges_algo/BlackbeardsHiddenTreasures/anglais1.png)

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-algo/3.Challenges/ressources_challenges_algo/BlackbeardsHiddenTreasures/BlackbeardsHiddenTreasures/){ .md-button target="_blank" rel="noopener" }


!!! note "Cadenas"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

         **Difficulté :** ⭐⭐ 5 flags

        **Objectif :** travailler les mécanismes de compréhension et de réflexion nécessaires à la mise en place des concepts abstraits de l'algorithmique.

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-algo/3.Challenges/ressources_challenges_algo/Cadenas/coffre_cadenas.png){: .center }
    
    Après des années de recherche, vous venez enfin de localiser un coffre contenant un incroyable trésor. Arrivé sur 
    place, vous vous apercevez que son système d'ouverture est incroyablement complexe et qu'il vous faudra user de toute 
    votre perspicacité pour résoudre les cinq puzzles de codage nécessaires...

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-algo/3.Challenges/ressources_challenges_algo/Cadenas/coffre_cadenas/){ .md-button target="_blank" rel="noopener" }
	
!!! note "Cadenas dynamiques"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐ 3 flags

        **Objectif :** travailler les mécanismes de compréhension et de réflexion nécessaires à la mise en place des concepts abstraits de l'algorithmique.

	![](https://cybersecurite.forge.apps.education.fr/cyber-2-algo/3.Challenges/ressources_challenges_algo/CadenasAlea/cadenas.png){: .center }

    La suite du challenge 
    <a href="https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/ADN/adn/" target="_blank">Un brin peut en cacher un autre</a>. Il n'est pas nécessaire de l'avoir terminé. Mais c'est la première apparition du petit Marius, l'assistant du Dr Kalifir.

    Dans ce nouveau challenge il va montrer l'étendue de ses compétences pour ouvrir ces cadenas, suscitant l'admiration du Dr Kalifir et du Dr Kalifounet.

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-algo/3.Challenges/ressources_challenges_algo/CadenasAlea/cadenas/){ .md-button target="_blank" rel="noopener"}
