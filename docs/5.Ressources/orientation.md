---
hide:
  - toc
author: à compléter
title: Orientation
---

# Les filières 

<figure>
<center><img src='../parcourscyber1.jpg' alt='image'></center>
</figure>

**Il n'y a pas un mais des chemins vers les métiers de la cybersécurité...**

Quelle que soit la voie choisie, il y a de nombreux parcours d'orientation envisageables et surtout de nombreuses possibilités d’évoluer et de trouver un emploi (selon l’OCDE en 2023 il **manque plus de 60 000 emplois** dans la cybersécurité).

## Au lycée (pré-bac)

!!! note "La voie professionnelle"

    La voie professionnelle avec le **bac Pro CIEL (Cybersécurité, informatique et électronique)** , pour une formation directement ancrée dans le numérique industriel (réseaux, objets connectés) et le monde professionnel, avec un objectif principal de devenir technicien en informatique (BTS CIEL, BUT…) et avec de nombreuses possibilités d’évoluer.

    - <a href="https://www.onisep.fr/ressources/univers-formation/formations/lycees/bac-pro-cybersecurite-informatique-et-reseaux-electronique" target="_blank">Bac Pro CIEL (Cybersécurité, informatique et électronique)</a>

!!! note "La voie technologique"

    La voie technologique avec la **STI2D (Sciences et Technologies de l’Industrie et du Développement Durable) option SIN** (systèmes d’infomration et numérique)  qui n’est pas spécifique à la cybersécurité mais qui par les usages du numérique et notamment de sécurité numérique peut préparer à des études universitaires (BTS CIEL ou SIO, BUT ou plus) dans la cybersécurité avec un bagage scientifique et technique.

!!! note "La voie générale"

    La voie générale avec la spécialité **Numérique et Sciences informatiques (NSI)** en classes de première et de terminale, ou bien avec des spécialités scientifiques en **sciences de l’ingénieur (SI)**, **mathématiques, sciences physiques** permettront d’envisager des études scientifiques plus longues avec un cursus d’ingénieur notamment (via des Classes préparatoires aux grandes écoles CPGE) filières Mathématiques/Physique MP ou Physique Sciences de l’ingénieur (PSI) notamment dans le numérique, l’informatique et la cybersécurité.

!!! tip "Remarque"

    La cybersécurité ce n’est pas que des compétences techniques ! Des spécialités de géopolitique ou encore des sciences de gestion en voie technologique peuvent ouvrir à de nouveaux postes dans des domaines de cybersécurité (analyse, anticipation et gestion de crise, vente de produits de cybersécurité, assurance, ressources humaines …). Au lycée il n’y a donc pas de voie si particulière vers la cybersécurité, tout est ouvert !


## Au niveau BAC

!!! note "Mention complémentaire Cybersécurité" 

    Juste après le baccalauréat vous pouvez passer une **« Mention Complémentaire » (MC)** notamment **Cybersécurité** pour vous permettre d’entrer rapidement dans le monde du travail et devenir technicien. C’est une possibilité rapide de débuter en cybersécurité et au regard des postes vacants c’est une vraie ouverture, y compris pour d’autres postes plus tard.  

!!! note "La Mention complémentaire NSO"

    La **Mention complémentaire NSO (Services numériques aux organisations)** permet également d’entrer rapidement dans une entreprise et de devenir technicien généraliste du numérique avec notamment des fonctions de sensibilisation à la sécurité numérique (mots de passe, mises à jour…).

## BAC+2 Les BTS

!!! note "BTS CIEL" 

    Le **BTS CIEL (Cybersécurité, informatique et électronique)** en 2 ans après le Bac, avec le développement de compétences en conception et intégration de systèmes informatiques et électroniques industriels (réseaux informatiques, objets connectés et automatisés, informatique embarquée). Cette formation mène à des métiers de technicien supérieur en cybersécurité notamment dans l’industrie et les services de communication dans les entreprises. Il est possible de poursuivre vers d’autres spécialités en Bac +3/+5 ou plus encore.

    - <a href="https://www.onisep.fr/ressources/univers-formation/formations/post-bac/bts-cybersecurite-informatique-et-reseaux-electronique-option-a-informatique-et-reseaux" target="_blank">BTS CIEL IR (informatique et réseaux)</a>
    - <a href="https://www.onisep.fr/ressources/univers-formation/formations/post-bac/bts-cybersecurite-informatique-et-reseaux-electronique-option-b-electronique-et-reseaux" target="_blank">BTS CIEL ER (électronique et réseaux)</a>

!!! note "BTS SIO" 

    Le **BTS SIO (Services informatiques aux organisations)** en deux ans après le Bac, avec deux options, « Solutions d’Infrastructure, Systèmes et Réseaux (SISR) » et « Solutions Logicielles et Applications Métiers (SLAM) ». SISR porte sur la gestion des réseaux et des systèmes informatiques, ainsi que sur la sécurité des données, SLAM sur le développement des logiciels et des applications web et mobiles et leurs sécurisation.  Cette formation mène à des métiers de technicien supérieur en cybersécurité notamment dans les services informatiques des entreprises ou en appui de service (consultant, développeur). Il est possible de poursuivre vers d’autres spécialités en Bac +3/+5 ou plus encore.

    - <a href="https://www.onisep.fr/ressources/univers-formation/formations/post-bac/bts-services-informatiques-aux-organisations-option-b-solutions-logicielles-et-applications-metiers" target="_blank">BTS SIO SLAM</a>
    - <a href="https://www.onisep.fr/ressources/univers-formation/formations/post-bac/bts-services-informatiques-aux-organisations-option-a-solutions-d-infrastructure-systemes-et-reseaux" target="_blank">BTS SIO SISR</a>

 
## Bac +3 et plus

Au niveau universitaire, dans des établissements publics et privés, il est recommandé de suivre des formations labellisées par l’ANSSI avec le <a href="https://cyber.gouv.fr/secnumedu" target="_blank">label SecNumEdu </a> ou le  <a href="https://www.cyberedu.fr/#:~:text=CyberEdu%20est%20un%20label%20g%C3%A9r%C3%A9%20par%20l'association%20et" target="_blank">label CyberEdu </a>.


!!! note "Les Bachelors" 

    Les Bachelors dont le **BUT (Bachelor Universitaire de Technologie) « Réseaux et Télécommunications » avec option Cybersécurité** est une spécialisation sur l’administration et la sécurisation des réseaux, préparant les étudiants à des carrières dans la sécurité informatique et la protection des réseaux.

    - <a href="https://www.onisep.fr/ressources/univers-formation/formations/post-bac/but-reseaux-et-telecommunications-parcours-cybersecurite" target="_blank">BUT parcours réseaux et télécommunications cyber</a>
    

!!! note "Licence Informatique"

    Licence Informatique avec parcours en cybersécurité : certaines universités proposent des **licences professionnelles spécialisées en sécurité des systèmes d’information** pour une spécialisation.


!!! note "Les masters"

    Certaines universités poursuivent la spécialisation en proposant un **master en cybersécurité**. Les masters offrent une expertise avancée dans le domaine de la cybersécurité, permettant aux étudiants de se spécialiser dans des domaines spécifiques tels que la cryptographie, l’analyse de malware, ou la sécurité des réseaux.

    - <a href="https://www.onisep.fr/ressources/univers-formation/formations/post-bac/master-mention-cybersecurite" target="_blank">Master mention cyber </a>
    

!!! note "Les écoles d’ingénieurs" 

    Des **écoles d’ingénieurs publiques et privées** proposent des diplômes d’ingénieur ou de spécialistes experts en cybersécurité (titres RNCP niveaux 6 et 7), via un cursus post bac de CPGE puis de concours aux écoles d’ingénieur ou de cursus intégrés dans l’école.

!!! tip "Remarque"

    De plus en plus d’universités ou d’écoles proposent des formations en alternance mais il faut aussi penser qu’avec un secteur ayant beaucoup de postes vacants il devient parfois difficile d’obtenir un poste d’apprenti dans une entreprise.

  
!!! warning "Attention"

    Enfin la **reconversion** est aussi un moyen de choisir les métiers de la cybersécurité. **Il faut avoir en tête qu’il n’y a pas une voie mais des voies pour s'orienter vers les métiers de la cybersécurité et ceci à tout moment.**


