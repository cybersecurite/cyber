---
hide:
  - toc
author: à compléter
title: Expressions régulières
---

# Expressions régulières

![](../images/regex.png){: .center }


!!! abstract "Présentation générale"

    En informatique, une **expression régulière**, également appelée **expression rationnelle**, **expression normale**, **motif** ou encore **regex** (fusion des deux mots anglais _regular expression_) est une chaîne de caractères qui décrit, selon une syntaxe précise, un ensemble de chaînes de caractères possibles. Elles sont principalement utilisées pour programmer des logiciels avec des fonctionnalités de lecture, de contrôle, de modification, et d'analyse de textes ainsi que dans la manipulation des langues formelles que sont les langages informatiques.



## Principe général

Les expressions régulières sont issues des théories mathématiques des langages formels des années 1940. Elles correspondent à des suites de caractères typographiques (appelées _pattern_ en anglais) décrivent un ensemble de chaînes de caractères. Par exemple l’ensemble de mots _ex équo_, _ex equo_, _ex aequo_ et _ex æquo_ peut être condensé en le seul motif `ex (a?e|æ|é)quo`. Les mécanismes de base pour former de telles expressions sont basés sur des caractères spéciaux (ou _opérateurs_) de substitutions, de groupement et de quantification.

## Principes de construction

Les **motifs simples** sont construits directement à partir de caractères pour lesquels on souhaite avoir une correspondance directe. Par exemple, le motif `des` correspond lorsqu'on observe exactement la chaîne de caractères `'des'`. On pourrait utiliser ce motif et détecter une correspondance dans les chaînes suivantes : `"J'ai fait des challenges ?"` et `"La description de ce challenge était compliquée"` car la chaîne de caractères `'des'` y est présente.

A noter que les expressions régulières sont **sensibles à la casse**. Ainsi, si le motif `des` donne une correspondance dans les deux chaînes précédentes, le motif `Des`, lui, n'en donne aucune.

Lorsque le motif à trouver est plus complexe qu'un motif simple, celui-ci devra contenir différents **caractères spéciaux (ou opérateurs)**. On en donne ci-dessous quelques-uns parmi les plus importants.

??? info "Opérateurs de substitutions"

    + `^` : recherche une correspondance au début d'une ligne. Par exemple `^a` recherche un `'a'` au début d'une ligne
    + `$` : recherche une correspondance en fin d'une ligne. Par exemple `a$` recherche un `'a'` en fin de ligne
    + `.` : recherche un caractère et un seul
    + `?` : correspond à l'expression précédente qui est présente une fois ou pas du tout. Par exemple, `e?le?` correspond aux chaînes `'ele'`, `'le'`, `'el'` et `'l'`
    + `*` : correspond à l'expression précédente qui est répétée 0 ou plusieurs fois. Par exemple, `bo*` correspond au caractère `'b'` auquel on ajoute 0 ou plusieurs fois le caractère `'o'` : `'b'`, `'bo'`, `'boo'`, etc.
    + `+` : correspond à l'expression précédente qui est répétée 1 ou plusieurs fois. Par exemple, `bo+` correspond à la chaîne `'bo'` à laquelle on ajoute 1 ou plusieurs fois le caractère `'o'` : `'bo'`, `'boo'`, `'booo'`, etc.
    + `|` : correspond à une alternative. Par exemple `vert|rouge` correspond à la chaîne `'vert'` ou à la chaîne `'rouge'`
    
??? info "Opérateurs de groupement"

    - `[`_liste_`]` : correspond à un des caractères de la liste _liste_ entre crochets. Par exemple :
        - `[aeiou]` correspond aux chaînes `'a'`, `'e'`, `'i'`, `'o'` et `'u'`
        - `[a-z]` correspond à un caractère minuscule entre `a` et `z`
        - `[AaBb]` correspond aux chaînes `'A'`, `'a'`, `'B'` et `'b'`
    - `[^`_liste_`]` : correspond à un caractères qui n'est pas l'un de ceux de la liste _liste_ entre crochets. Par exemple, `[^aeiou]` correspond à un caractère qui n'est pas une voyelle en minuscule
    - `(`_expr_`)` : correspond à l'expression _expr_ entre parenthèse. Par exemple, `(détecté)` décrit les chaînes `'détecté` et `détectés`, mais pas la chaîne `'détecta'`

??? info "Opérateurs de quantification"

    - _expr_`{`_n_`}` : correspond à exactement _n_ occurrences de l’expression _expr_ précédant les accolades. Par exemple `a{3}` correspond à la chaîne `'aaa'`
    - _expr_`{`_n_`,`_m_`}` : correspond à avoir entre _n_ et _m_ occurrences de l’expression _expr_ précédant les accolades. Par exemple, `a{2,4}` correspond aux chaînes `'aa'`, `'aaa'` et `'aaaa'`
    - _expr_`{`_n_`,}` : correspond à au moins _n_ occurrences de l’expression _expr_ précédant les accolades. Par exemple, `a{2}` correspond aux chaînes constituées de deux `a` ou plus : `'aa'`, `'aaa'`, `'aaaa'`, `'aaaaa'`, etc.

        

??? example "Exemples" 

    En combinant toutes ces règles, on peut décrire des expressions très compliquées. Par exemple :

     - `[0-9]{3}` correspond à une chaîne de trois caractères qui sont des chiffres entre 0 et 9
     - `[0-9a-fA-F]{4,8}` correspond à une chaîne entre 4 et 8 caractères qui ne contient que des caractères hexadécimaux
     - `[0-9]{3}[ -][a-z]{3}[ -][Ll].*k` correspond à une chaîne de caractères de trois chiffres, suivis par un espace ou un tiret, suivis par trois lettres en minuscules, suivis par un espace ou un tiret, suivi par un seul caractère L en majuscule ou minuscule, suivi de zéro ou plusieurs caractères de n'importe quel type, suivis par un k minuscule

    On trouvera d'autres exemples de manipulation sur les deux pages suivantes :

    - <a href='http://www.lexique.org/?page_id=101' target='_blank'>Petit lexique des expressions régulières</a>
    - <a href='http://regextutorials.com/intro.html' target='_blank'>Petits exercices de manipulation des expressions régulières</a>



## Quelques utilisations pratiques

Les expressions régulières sont utilisées de façon très importante en informatique.

Par exemple, lorsque l'on construit un formulaire, on peut demander à l'utilisateur de saisir des informations satisfaisant à une certains critères. On utilise pour cela l'attribut `pattern` qui contiendra, sous la forme d'une chaîne de caractères, une expression régulière que doit respecter la valeur du champ afin d'être valide. Ainsi, le code
<center>`<input type='password' pattern='[A-Z]{2,}[a-z]{2,}[0-9]{3,}[!?&#@%$]{1,}'>`</center><br>
définit une zone de saisie de mot de passe s'écrivant avec, dans cet ordre, au moins deux majuscules, deux minuscules, trois chiffres et un caractère spécial.

L'expression régulière saisie dans l'attribut `pattern` doit être une expression régulière valide pour JavaScript telle qu'utilisée par la classe <a href='https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/RegExp' target='_blank'>`RegExp`</a>.

Pour définir une expression régulière en JavaScript, c'est-à-dire une instance de la classe `RegExp`, il suffit de la délimiter par des barres obliques (_slashes_). Ainsi, l'instruction
<center>`const exp_reg = /^[A-Z]{2,}[a-z]{2,}[0-9]{3,}[!?&#@%$]{1,}$/`</center><br>
définie la même expression régulière que dans l'exemple précédent.

Une fois l'expression régulière définie, on peut lui appliquer diverses méthodes. La plus simple est la méthode `test` prenant en paramètre une chaîne de caractères et renvoyant `true` s'il y a correspondance, `false` sinon. Ainsi, en utilisant l'expression régulière `exp_reg` précédente :

 - l'instruction `exp_reg.test('AZFbtj547893!@')` renvoie `true` ;
 - l'instruction `exp_reg.test('Fetg147!')` renvoie `false` (il manque une majuscule) ;
 - l'instruction `exp_reg.test('AZb85erB@46')` renvoie `false` (les éléments ne sont pas dans le bon ordre).

Bien sûr, les expressions régulières ne sont pas uniquement réservées aux formulaires et au JavaScript. Par exemple, en Python, le module <a href='https://docs.python.org/fr/3/library/re.html#' target='_blank'>`re`</a> permet de gérer ces expressions (voir aussi ce <a href='https://docs.python.org/fr/3/howto/regex.html' target='_blank'>lien</a> pour plus de détails sur les expressions régulières en Python).