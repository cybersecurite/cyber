---
hide:
  - toc
author: à compléter
title: Try Hack Me
---

# Try Hack Me

![](../images_sites_challenges/Plateforme_TryHackMe.png){: .center }




!!! abstract "Présentation générale de la plateforme"

    **Try Hack Me** est une plateforme adaptée aux débutants en informatique et à ceux qui veulent s'initier à la cybersécurité en étant très guidé dans la résolution du problème. Elle apprend, par exemple, les bases de l’utilisation d’un terminal, des systèmes UNIX et des réseaux. C'est une plateforme de qualité où chaque module proposé est complet et précis.


    - **Type de challenges :** généraliste
    - **Niveau des challenges :** débutant
    - **Lien vers la plateforme :** <a href='https://tryhackme.com/' target='_blank'>https://tryhackme.com/</a>