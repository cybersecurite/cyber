---
hide:
  - toc
author: à compléter
title: Hackropole
---

# Hackropole

![](../images_sites_challenges/Plateforme_Hackropole.png){: .center }




!!! abstract "Présentation générale de la plateforme"

    **Hackropole** est plateforme proposée par l'<a href='https://cyber.gouv.fr/' target='_blanck'>ANSSI</a> et regroupant l'intégralité des épreuves du France Cybersecurity Challenge (FCSC). Elle permet de découvrir et de se former à divers domaines de la cybersécurité. 


    - **Type de challenges :** généraliste
    - **Niveau des challenges :** bon niveau
    - **Lien vers la plateforme :** <a href='https://hackropole.fr/fr/' target='_blank'>https://hackropole.fr/fr/</a>