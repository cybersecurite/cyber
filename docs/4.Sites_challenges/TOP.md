---
hide:
  - toc
author: à compléter
title: TOP - The Osint Project
---

# TOP - The Osint Project

![](../images_sites_challenges/The-Osint-Project.png){: .center }




!!! abstract "Présentation générale de la plateforme"

    **TOP - The Osint Project** est une plateforme réalisée par le <a href='https://campuscyber.fr/' target='_blank'>Campus Cyber</a> et entièrement dédiée à la pratique de l'OSINT. On y trouve de nombreux challenges de difficulté croissante pour progresser, des contenus pédagogiques sur des outils ou des méthodologies spécifiques à l'OSINT, ainsi que des outils pour la pratique, des informations et des conseils.


    - **Type de challenges :** OSINT
    - **Niveau des challenges :** bon niveau
    - **Lien vers la plateforme :** <a href='https://the-osint-project.fr/' target='_blank'>https://the-osint-project.fr/</a>