---
hide:
  - toc
author: à compléter
title: Ressources pour approfondir
---

# Ressources pour approfondir



!!! abstract "Explications de la partie"
    à compléter



La réduction des risques induits par l’usage des technologies numériques repose d’abord sur le respect de bonnes pratiques à adopter. Les <a href="https://cyber.gouv.fr/dix-regles-dor-preventives" target="_blank">Dix règles d'or préventives</a> à connaitre.


Pour informer et sensibiliser les publics sur les menaces numériques, le dispositif **Cybermalveillance.gouv.fr** met à disposition divers contenus thématiques comme par exemple :

- Le chantage à l’ordinateur ou à la webcam prétendus piratés 
- L’escroquerie aux faux ordres de virement (FOVI)
- La fuite ou violation de données personnelles
- L’hameçonnage (phishing en anglais)

<a href="https://www.cybermalveillance.gouv.fr/tous-nos-contenus/actualites/liste-des-ressources-mises-a-disposition" target="_blank">Liste des ressources mises à disposition par le dispositif Cybermalveillance.gouv.fr</a>


!!! abstract "Comprendre les menaces et adopter les bonnes pratiques !"

    Découvrez les mécanismes des principales menaces sur Internet et apprenez à mieux vous en protéger.
    A l'issue de l’e-sensibilisation une attestation de suivi vous sera remise.

    Ce programme de e-sensibilisation est un parcours de sensibilisation simple en ligne ouvert à tout le monde.
    A noter ce même programme est adapté pour les enseignants sur <a href="https://magistere.education.fr/local/magistere_offers/index.php?v=formation#offer=1266" target="_blank">M@gistère avec des ressources</a> et un <a href="https://www.cybermalveillance.gouv.fr/sens-cyber/apprendre" target="_blank">podcast
    Programme de l'e-sensibilisation</a>.





Dans le cadre de sa mission de sensibilisation, l’ANSSI propose <a href="https://cyber.gouv.fr/le-mooc-secnumacademie" target="_blank">SecNumacadémie</a> pour former le plus grand nombre de citoyens à la sécurité du numérique.

Un <a href="https://app.pix.fr/campagnes/MKWWVM413" target="_blank">parcours Pix</a> a également été créé spécifiquement sur la sécurité numérique.

Enfin, le podcast <a href="https://podcast.ausha.co/la-cyber-securite-expliquee-a-ma-grand-mere" target="_blank">La cybersécurité expliquée à ma grand-mère</a> est intéressant pour aller plus loin.


