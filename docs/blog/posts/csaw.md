---
title: CSAW Red Team 2024
date: 2024-08-19
categories:
  - Général
  - Actions
---

# CSAW Red Team 2024


📢👨‍🏫 Chers enseignants en SI / NSI,

🚀 Vous êtes passionnés par la sécurité informatique et vous souhaitez donner à vos élèves l'opportunité de se familiariser avec ce domaine en pleine expansion ?


![](images_posts/csaw2024.png)



<!-- more -->

 🔐 Nous vous invitons à découvrir la Red Team Competition, une compétition de cybersécurité spécialement conçue pour les lycéens, dans le cadre du concours CSAW (Cyber Security Awareness Week) qui aura lieu les 7 et 8 novembre 2024. 
 
 📅 Pour en savoir plus sur cette compétition excitante et obtenir tous les détails pour y participer, rejoignez-nous lors d'une réunion Zoom le 18 septembre à 16h30 avec le lien ci-dessous :

🔗[reunion-presentation-csaw24-red-team](https://framaforms.org/reunion-presentation-csaw24-red-team-1714730402)


