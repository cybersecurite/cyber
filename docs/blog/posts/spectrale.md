---
title: Nouveau challenge
date: 2024-07-21
categories:
  - Ressources
---

![](images_posts/image_spectrale.png)

<a href='>https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/Spectrale/spectrale/' target='_blank'>Spectrale</a> :
un groupe de hackers astucieux orchestrent des attaques informatiques complexes et apparemment indétectables.

<!-- more -->