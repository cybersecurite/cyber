---
title: Passe ton hack d’abord 2025
date: 2024-10-01
categories:
  - Général
  - Actions
---

#   Webinaire de lancement de lancement du challenge lycéen Passe ton hack d’abord 2025

Le webinaire de lancement du prochain challenge lycéen de cybersécurité, Passe ton hack d'abord 2025, se tiendra le mardi 08 octobre de 11 à 12h et il sera enregistré.


<!-- more -->

Si vous désirez y participer, vous pouvez vous inscrire à cette adresse :
<a href="https://grist.numerique.gouv.fr/o/docs/forms/qfg1ZsPhzGEhsgVmrX4Jw3/75" target="_blank">Passe ton hack d’abord 2025</a>

Le challenge sera organisé du **20 janvier au 07 février 2025** au niveau national avec une ouverture aux lycées français de l’étranger comme l’année dernière.