---
title: Nouveau challenge
date: 2024-09-21
categories:
  - Ressources
---

![](images_posts/cadenas.png)

<a href='https://cybersecurite.forge.apps.education.fr/cyber-2-algo/3.Challenges/ressources_challenges_algo/CadenasAlea/cadenas/' target='_blank'>Cadenas dynamiques</a> :
La suite du challenge <a href='https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/ADN/adn/' target='_blank'>Un brin peut en cacher un autre</a>. Il n'est pas nécessaire de l'avoir terminé, mais c'est la première apparition du petit Marius, l'assistant du Dr Kalifir.

Dans ce nouveau challenge il va montrer l'étendue de ses compétences pour ouvrir ces cadenas, suscitant l'admiration du Dr Kalifir et du Dr Kalifounet.

<!-- more -->
