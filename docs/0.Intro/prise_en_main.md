---
hide:
  - toc
author: à compléter
title: Prise en main
---

# Prise en main

![](../images_intro/menu.png){: .center }

Le site est organisé en six grandes sections :

## Actualités

Cette section regroupe de nombreuses informations sur la cybersécurité comme, par exemple, des actions de sensibilisation et d'initiation à la cybersécurité, des présentations de concours de CTF ou des ressources pédagogiques. **Toutes ces informations sont actualisées régulièrement.**


## Bonnes pratiques

Cette section présente différentes ressources sur **les bonnes pratiques en cybersécurité**, comme l'utilisation de mots de passe forts, la mise à jour régulière des logiciels et la sensibilisation des utilisateurs aux risques potentiels, afin de 
réduire les vulnérabilités et prévenir les attaques informatiques.


## Boîte à outils

Cette section présente **différents logiciels et fiches ressources** pouvant être utiles pour résoudre les challenges 
proposés.



## Challenges

 Cette section propose différents challenges CTF (Capture The Flag) dont la résolution passe par la détermination 
 d'un ou plusieurs &laquo; drapeaux &raquo; ou &laquo; flag &raquo; qui sont des morceaux de données ou de texte cachés 
 dans des systèmes informatiques. Ces défis peuvent inclure la recherche de failles de sécurité, la résolution de puzzles cryptographiques, ou l'utilisation de logiciels pour obtenir des réponses. A travers un environnement ludique vous pouvez tester et approfondir vos connaissances.

  Ces challenges sont regroupées dans différentes catégories :

 - Algorithmique
 - Cryptographie
 - OSINT
 - Programmation Python/Scratch
 - Réseaux
 - Stéganographie
 - Web - client
 - Web - serveur
 
 Pour chaque challenge, et à titre indicatif, sont indiqués le niveau nécessaire pour résoudre le challenge, la difficulté (facile, intermédiaire ou difficile) représentée par un nombre d'étoiles entre 1 et 3, le nombre de flags à trouver, ainsi qu'une description de l'objectif du challenge.

Par exemple, l'image ci-dessous montre que l'on a affaire à un challenge conçu plutôt pour des spécialités NSI, qu'il est de difficulité intermédiaire, que l'on a 12 flags à trouver et qu'il va permettre de travailler sur les notions de base de l'algorithmique, ainsi que sur les chaînes de caractères.

<center><img src='../images_intro/image_presentation_chall.png' alt='exemple challenge'></center>


## Plateformes de challenges 

Il existe de nombreuses plateformes de CTF de grandes qualités pour s'initier à la cybersécurité. Dans cette section, on en présente quelques-unes qui sont intéressantes, à la fois pour les types de challenges proposés, mais aussi pour l'approche 
et le contenu pédagogique sous-jacent.


## Divers

Dans cette section, on trouvera différentes ressources bibliographiques et sitographiques permettant d'approfondir ce qui est abordé dans ce site, ainsi qu'un point sur les métiers de la cybersécurité et les études pour y parvenir.